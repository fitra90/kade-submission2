package com.example.gbh61a.serieamatches.api

import android.net.Uri
import com.example.gbh61a.serieamatches.BuildConfig

object TheSportDBApi {

    //get next 15 events serie a italian
    //https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php?id=4332
    fun getNextMatches(): String {
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("api")
                .appendPath("v1")
                .appendPath("json")
                .appendPath(BuildConfig.TSDB_API_KEY)
                .appendPath("eventsnextleague.php")
                .appendQueryParameter("id", "4332")
                .build()
                .toString()
    }

    //get last 15 events serie a italian
    //https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4332
    fun getLastMatches(): String {
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("api")
                .appendPath("v1")
                .appendPath("json")
                .appendPath(BuildConfig.TSDB_API_KEY)
                .appendPath("eventspastleague.php")
                .appendQueryParameter("id", "4332")
                .build()
                .toString()
    }

    //get event detail
    //https://www.thesportsdb.com/api/v1/json/1/lookupevent.php?id=441613
    fun getMatchDetail (matchId: String?): String {
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("api")
                .appendPath("v1")
                .appendPath("json")
                .appendPath(BuildConfig.TSDB_API_KEY)
                .appendPath("lookupevent.php")
                .appendQueryParameter("id", matchId)
                .build()
                .toString()
    }
}