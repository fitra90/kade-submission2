package com.example.gbh61a.serieamatches.model

import com.google.gson.annotations.SerializedName

data class LastMatch(
        @SerializedName("idTeam")
        var teamId: String? = null,

        @SerializedName("strTeam")
        var teamName: String? = null,

        @SerializedName("strTeamBadge")
        var teamBadge: String? = null
)