package com.example.gbh61a.serieamatches.model

data class LastMastchResponse(
        val lastMatches: List<LastMatch>)